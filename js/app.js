const $deck = $(".deck");
const $timer = $(".js-timer");
const $moveCount = $(".js-moves");
const $score = $(".fa-star");
const cards = [
  "anchor",
  "anchor",
  "bomb",
  "bomb",
  "bicycle",
  "bicycle",
  "bolt",
  "bolt",
  "cube",
  "cube",
  "diamond",
  "diamond",
  "leaf",
  "leaf",
  "paper-plane-o",
  "paper-plane-o",
];

let shownCards = [];
let moves = 0;
let matches = 0;

function newGame() {
  // Reset HTML
  $("body").removeClass("no-scroll");
  $(".modal-bg").addClass("hidden");
  $(".fa-star-o").toggleClass("fa-star-o fa-star");
  $timer.html("0");
  $deck.empty();
  $moveCount.html("0");

  // Reset variables
  shownCards = [];
  moves = 0;
  matches = 0;

  // Append shuffled cards
  shuffle(cards);
  cards.forEach(function(card) {
    $deck.append((`<li class="card"><i class="fa fa-${card}"></i></li>`));
  });

  // Listen for card events
  cardListener();
};

function shuffle(array) {
  for (let i = array.length - 1; i > 0; i--) {
    // random index from 0 to i
    let j = Math.floor(Math.random() * (i + 1));
    // swap elements array[i] and array[j]
    [array[i], array[j]] = [array[j], array[i]];
  }
}

// Timer
let second = 0;
let started = false;
let timer;

function startTimer() {
  timer = setInterval(function stopWatch() {
    second++;
    $timer.html(second);
  }, 1000);

  started = true;
}

function stopTimer() {
  clearInterval(timer);
  second = 0;
  started = false;
}

function cardListener() {
  // Only the cards which are not matched/open can be clicked
  $deck.find('.card:not(".match, .open")').on("click", function(event) {
    // Store the clicked card for matching
    const clickedCard = $(this).children("i").attr("class");

    if (started == false) {
      startTimer();
    }

    // Only 2 cards can be shown at a time and opened cards can't be clicked again for a match
    if (($(".show").length == 2) || ($(this).hasClass("open"))) {
      return;
    };

    $(this).addClass("open show");

    shownCards.push(clickedCard);

    if (shownCards.length > 1) {
      if (clickedCard == shownCards[0]) {
        $deck.find(".open").addClass("match animated bounce");

        setTimeout(function() {
          $deck.find(".open").toggleClass("open show animated bounce");
        }, 1000);

        shownCards = [];
        moves++
        matches++

        $moveCount.html(moves);
        setRating(moves);
      } else {
        $deck.find(".open").addClass("unmatch animated wobble");

        setTimeout(function() {
          $deck.find(".open").toggleClass("open show unmatch animated wobble");
        }, 1000);

        shownCards = [];
        moves++

        $moveCount.html(moves);
        setRating(moves);
      }
    }

    if (matches == 8) {
      endGame();
    }
  });
};

// Score
let starRating = 3;

function setRating(moves) {
  if (moves > 15) {
    $score.eq(2).removeClass("fa-star").addClass("fa-star-o");
    starRating = 2;
  };

  if (moves > 20) {
    $score.eq(1).removeClass("fa-star").addClass("fa-star-o");
    starRating = 1;
  };
};

// End game
function endGame() {
  $(".js-end-moves").html(moves);
  $(".js-end-time").html(second);
  $(".js-end-score").html(starRating);
  $(".modal-bg").removeClass("hidden");
  $("body").addClass("no-scroll");

  stopTimer();
}

// Restart game
$(".js-restart").on("click", function() {
  stopTimer();
  newGame();
});

$(".js-replay").on("click", function() {
  stopTimer();
  newGame();
});

// Quit game
$(".js-quit").on("click", function () {
  $(".modal-bg").addClass("hidden");
  $("body").removeClass("no-scroll");
});

newGame();
