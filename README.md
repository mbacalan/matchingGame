[![Netlify Status](https://api.netlify.com/api/v1/badges/b1731799-c997-465d-b34a-9ba9d36abe5a/deploy-status)](https://app.netlify.com/sites/cardmatcher/deploys)

# Memory (Matching) Game

A browser based memory/matching game using JavaScript.

The game starts when the player makes their first attempt at finding a matching pair of cards. The matching cards will stay open and the game ends when the player discovers all the matches. Cards are shuffled for every game.

Players are rated by the amount of moves it took them to find all the matches.

* \<15 moves = 3 stars
* \<20 moves = 2 stars
* \>20 moves = 1 star

## Dependencies

* [jQuery](http://jquery.com/)
* [Font Awesome](https://fontawesome.com/)
* [Animate.css](https://daneden.github.io/animate.css/)

## Notes

This project is built upon the base code provided by [Udacity](https://www.udacity.com/) under the Front-End Web Developer Nanodegree.
